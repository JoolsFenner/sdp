package Test;

import reflection.Person;

abstract class TestClass {
	public static Person create() throws Exception {
		String className = System.getProperty(null, null);
		Class c = Class.forName(className);
		
		return (Person)c.newInstance();
	}
	
//	abstract void op1(...);
//	
//	abstract void op2(...);
}