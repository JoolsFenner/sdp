package reflection;

public class Person {
	private String name;
	private String userid;
	
	/*
	 * @param name
	 * @param userid
	 */
	
	public Person(){
		this("Julian","Fenner");
	}
	
	public Person(String name, String userid) {
		super();
		this.name = name;
		this.userid = userid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	private void privateMethod(){}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", userid=" + userid + "]";
	}
	
	

}
