package reflection;

import java.lang.reflect.Method;
import java.util.Scanner;

/*
 * http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
 */

public class MyClass {

	public static void main(String[] args) {
		new MyClass().launch();
	}

	private void launch() {
		getClassFromConsole();
		getMethodsTest();
	}
	
	private void getClassFromConsole(){
		// try with resource statement
		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Input here: ");
			String str = sc.next();
			
			Class<?> c = Class.forName(str);
			Object obj = c.newInstance();
			System.out.println(obj);
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private void getMethodsTest(){
		Class<?> c = null;
		try {
			c = Class.forName("java.util.Random");
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Method[] m  = c.getMethods();
		Method[] dm  = c.getDeclaredMethods(); // shows private methods
		
		System.out.println("-----getMethods(): ------");
		
		for(Method next: m){
			System.out.println(next);
		}
		
		System.out.println("-----getDeclaredMethods(): ------");
		for(Method next: dm){
			System.out.println(next);
		}
	}

	
}