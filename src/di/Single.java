package di;


/*
 * @Singleton annotation adds this to class.  You don't need to write
 * Guarantees you only ever get one, always returns the same reference
 */
public class Single {
	
	private static Single single = null;
	
	static{
		single = new Single();
	}
	
	public static Single getInstance(){
		return single;
	}

}
