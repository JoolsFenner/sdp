package feb19Java8_2;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PickFirst {

	public static void main(String[] args) {
		List<String> friends = Arrays.asList("Betty","Bob","James", "Frank","Liam");
		
		
		//optional for handling case of starts with returning nothing
		Optional<String> foundName =
				friends.stream()
				.filter(name -> name.startsWith("Z"))
				.findFirst();
				
		System.out.println("A name starting with F: " + foundName.orElse("was not found"));

	}

}
