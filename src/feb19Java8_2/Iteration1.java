package feb19Java8_2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/*
 * https://docs.oracle.com/javase/8/docs/api/java/util/function/Function.html
 */

public class Iteration1 {

	public static void main(String[] args) {
		List<String> friends = Arrays.asList("Betty","Bob","James");
		
		//friends.forEach(new MyConsumer<>());
		
//		anonymous inner class		
//		friends.forEach(new Consumer<String>(){
//			public void accept (String t){
//				System.out.println(t);
//			}
//		});
		
//		//functional
//		friends.forEach( (String name) -> System.out.println(name));
//		//alternative
//		friends.forEach( name -> System.out.println(name));
//		//alternative
//		friends.forEach(System.out::println);
//		// all do the same, syntactic sugar
		
		
		friends.stream()
		       .map(name -> name.toUpperCase())
		       .forEach(System.out::println);
		
		friends.stream()
	       .map(name -> name.length())
	       .forEach(count -> System.out.println(count));

	}

}

//class MyConsumer<T> implements Consumer<T>{
//	public void accept (T t){
//		System.out.println(t);
//	}
//}