package feb19Java8_2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;



public class PickNames {

	public static void main(String[] args) {
		List<String> friends = Arrays.asList("Betty","Bob","James");
		
		//https://docs.oracle.com/javase/8/docs/api/java/util/function/Predicate.html
		Predicate<String> startsWithB = 
				name -> name.startsWith("B");
				
		friends.stream()
		       //.filter(name -> name.startsWith("B"))
			   .filter(startsWithB)
			   .forEach(System.out::println);
		
		friends.stream()
		       .mapToInt(name -> name.length())
		       .sum(); //.sum() is example of terminator
		
		String s = 
				friends.stream()
				.reduce("Fred",  //identity operator
						(name1,name2) ->
		                       name1.length() >= name2.length() ? name1: name2); 

		System.out.println(s);
		
		
	}

}
