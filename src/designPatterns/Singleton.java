package designPatterns;

/*
 * http://www.tutorialspoint.com/design_pattern/singleton_pattern.htm
 * 
 * Singleton pattern is one of the simplest design patterns in Java. This type of design pattern 
 * comes under creational pattern as this pattern provides one of the best ways to create an object.
 *This pattern involves a single class which is responsible to create an object while making sure that 
 *only single object gets created. This class provides a way to access its only object which can be 
 *accessed directly without need to instantiate the object of the class.*/


class SingleObject{
	
	private static SingleObject instance = new SingleObject();
	
	//constructor is made private so that this class cannot be instantiated
	private SingleObject(){}

	//Get the only object available
	public static SingleObject getInstance(){
		return instance;
	}
	
	public void showMessage(){
		
		System.out.println("Hello I'm a singleton, I'm the only object in this Singleton class");
	}

}


public class Singleton {

	public static void main(String[] args){
		// Illegal term is constuctor SingleObject() is not visible
		//SingleObject object = new SingleObject();
		
		SingleObject object = SingleObject.getInstance();
		
		object.showMessage();
		
		
	}
	
}


