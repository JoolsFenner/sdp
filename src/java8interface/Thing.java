package java8interface;

public interface Thing {
	//if this isn't overridden this is implemented
	public default boolean alive(){
		return false;
	}
}

interface Fly extends Thing{
	public String eat();
}


interface Person extends Thing{
	public String eat();
}


interface Huly extends Fly, Person{
	
}


class FlyImpl implements Fly{

	@Override
	public boolean alive() {
		return false;
	}

	@Override
	public String eat() {
		// TODO Auto-generated method stub
		return "Vomit absorb... repeat";
	}
	
	
}

class PersonImpl implements Person{

	@Override
	public boolean alive() {
		return true;
	}

	@Override
	public String eat() {
		return "chew and swallow";
	}
	
	
}


class HulyImpl implements Huly{
	private PersonImpl pi;

	@Override
	public String eat() {
		// TODO Auto-generated method stub
		return pi.eat();
	}

	
}