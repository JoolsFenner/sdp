package feb19Java8;

import java.util.Arrays;
import java.util.List;

public class Cities {
	
	public static void main(String[] args){
		List<String> cities = Arrays.asList("NYC", "Chicago","Frankfurt");
		
		findCityImperative(cities, "Chicago");
		findCityFunctional(cities, "Chicago");
	}
	
	
	private static void findCityFunctional(final List<String> cities, String name) {
		System.out.println("Found " + name + " ?: " + cities.contains(name));
	}


	public static void findCityImperative(final List<String> cities, String name){
		boolean found = false;
		for (String city : cities)
			if(city.equals(name)){
				found = true;
				break;
			}
		
		System.out.println("Found " + name + " ?: " + found);
	}
}
