package feb19Java8;

public interface A {
	public default String eats(){
		return "I always eat";
	}

	public default void anotherOne(){}
	
}